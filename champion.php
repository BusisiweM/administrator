<?php session_start();
    include_once "php/dbconfig.php";
    error_reporting(E_ALL & ~E_NOTICE);


/*Code for searching and viewing data*/
if(isset($_POST['search'])){
    $d = $_POST['search_text'];
    $sql = "SELECT  * from champions where champion_id = '$d'";
    $result = $con-> query($sql);
    if ($result-> num_rows > 0){
        while ($row = $result-> fetch_assoc()){

            $champions_id = $row['champion_id'];
            $name = $row['names'];
            $surname = $row['surnames'];
            $number = $row['phone_number'];
            $email = $row['email'];
            $id_number = $row['id_number'];
            $age = $row['age'];
            $gender = $row['gender'];
            $address = $row['address'];
            $province = $row['province'];
            $coordinator_id = $row['coordinator_id'];
            $qualification = $row['qualification'];

        }      
    }
    else
    {
        echo "0 results";
    }
}

if(isset($_GET['edit'])){
    $d = $_GET['edit'];
    $sql = "SELECT  * from champions where champion_id = '$d'";
    $result = $con-> query($sql);
    if ($result-> num_rows > 0){
        while ($row = $result-> fetch_assoc()){

            $champions_id = $row['champion_id'];
            $name = $row['names'];
            $surname = $row['surnames'];
            $number = $row['phone_number'];
            $email = $row['email'];
            $id_number = $row['id_number'];
            $age = $row['age'];
            $gender = $row['gender'];
            $address = $row['address'];
            $province = $row['province'];
            $coordinator_id = $row['coordinator_id'];
            $qualification = $row['qualification'];

        }      
    }
    else
    {
        echo "0 results";
    }
}

/*Code for updating */
if(isset($_POST['update'])){
$id = $_POST['champions_id'];

$sql = "UPDATE champions SET champion_id = '$_POST[champions_id]', names= '$_POST[names]', surnames = '$_POST[surname]', phone_number = '$_POST[phone_number]',
        email='$_POST[email]', age='$_POST[age]', gender='$_POST[gender]', address = '$_POST[address]', province = '$_POST[province]', qualification = '$_POST[qualification]' WHERE champion_id = '$id'";


if (mysqli_query($con, $sql)) {
    echo "<script> alert('You have updated your details!!')</script>";
} else {
    echo "<script> alert('Couldn't Update Your Details!!')</script>";
}
}

/** Code for delete */

if(isset($_GET['a'])){
$d = $_GET['a'];
$sql_d = "DELETE from champions where champion_id = '$d'";
if(mysqli_query($con, $sql_d)){

    echo "<script> alert('The record has been deleted')</script>";
}
else
{
    echo "<script> alert('Not deleted') </script>";
}
}


if(isset($_POST['insert'])){
    // receiving all input values from the form
    $champion_id = $_POST['champions_id'];
    $name = $_POST['names'];
    $surname = $_POST['surname'];
    $phone_number = $_POST['phone_number'];
    $email = $_POST['email'];
    $id_number = $_POST['id_number'];
    $age = $_POST['age'];
    $gender = $_POST['gender'];
    $address = $_POST['address'];
    $province = $_POST['province'];
    $coordinator_id = $_POST['coordinator_id'];
    $qualification = $_POST['qualification'];
    

    $sql = "INSERT INTO champions (champion_id,names, surnames, phone_number, email, id_number, age, gender, address,province, coordinator_id, qualification) 
    VALUES ('$champion_id','$name', '$surname','$phone_number', '$email', '$id_number', '$age','$gender', '$address','$province', '$coordinator_id', '$qualification')";
        
    if (mysqli_query($con,$sql)){
        echo "<script> alert('Champion Added')</script>";
 
    }else{
        echo "<script> alert('Failed')</script>";
    }
}

?>

<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>D4IR Initiative</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="images/favicon.png">
    <!-- Pignose Calender -->
    <link href="./plugins/pg-calendar/css/pignose.calendar.min.css" rel="stylesheet">
    <!-- Chartist -->
    <link rel="stylesheet" href="./plugins/chartist/css/chartist.min.css">
    <link rel="stylesheet" href="./plugins/chartist-plugin-tooltips/css/chartist-plugin-tooltip.css">
    <!-- Custom Stylesheet -->
    <link href="css/style.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

</head>

<body>

    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    
    <!--**********************************
        Main wrapper start
    ***********************************-->
    <div id="main-wrapper">

        <!--**********************************
            Nav header start
        ***********************************-->
        <div class="nav-header">
            <div class="brand-logo">
                <a href="index.html">
                    <b class="logo-abbr"><img src="images/logo.png" alt=""> </b>
                    <span class="logo-compact"><img src="./images/logo-compact.png" alt=""></span>
                    <span class="brand-title">
                        <img src="images/logo-text.png" alt="">
                    </span>
                </a>
            </div>
        </div>
        <!--**********************************
            Nav header end
        ***********************************-->

        <!--**********************************
            Header start
        ***********************************-->
        <div class="header">    
            <div class="header-content clearfix">
                
                <div class="nav-control">
                    <div class="hamburger">
                        <span class="toggle-icon"><i class="icon-menu"></i></span>
                    </div>
                </div>
                <div class="header-left">
                    <div class="input-group icons">
                        <form action="champion.php" method = "post" >
                            <div class="input-group-prepend">
                                <input type="text" class="form-control" name="search_text" placeholder="Search Dashboard" aria-label="Search Dashboard">
                                <span class="input-group-text bg-transparent border-0 pr-2 pr-sm-3" id="basic-addon1">
                                <button type="submit" name="search" value ="SEARCH" class="btn btn-primary"><i class="mdi mdi-magnify"></i></button></span>
                            </div>    
                        </form>
                      
                        <div class="drop-down animated flipInX d-md-none" >
                            <form action="#" method = "post" >
                                <input type="text" class="form-control" placeholder="Search">
                               
                            </form>
                        </div>
                    </div>
                </div>
                <div class="header-right">
                    <ul class="clearfix">
                        <li class="icons dropdown"><a href="javascript:void(0)" data-toggle="dropdown">
                                <i class="mdi mdi-email-outline"></i>
                                <span class="badge badge-pill gradient-1">1</span>
                            </a>
                            <div class="drop-down animated fadeIn dropdown-menu">
                                <div class="dropdown-content-heading d-flex justify-content-between">
                                    <span class=""> New Messages</span>  
                                    <a href="javascript:void()" class="d-inline-block">
                                        <span class="badge badge-pill gradient-1">3</span>
                                    </a>
                                </div>
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li class="notification-unread">
                                            <a href="javascript:void()">
                                                <img class="float-left mr-3 avatar-img" src="images/avatar/1.jpg" alt="">
                                                <div class="notification-content">
                                                    <div class="notification-heading">Name & Surname</div>
                                                    <div class="notification-timestamp">Time</div>
                                                    <div class="notification-text">Message will display  here ...</div>
                                                </div>
                                            </a>
                                        </li>
                                        
                                    </ul>
                                    
                                </div>
                            </div>
                        </li>
                        <li class="icons dropdown"><a href="javascript:void(0)" data-toggle="dropdown">
                                <i class="mdi mdi-bell-outline"></i>
                                <span class="badge badge-pill gradient-2">3</span>
                            </a>
                            <div class="drop-down animated fadeIn dropdown-menu dropdown-notfication">
                                <div class="dropdown-content-heading d-flex justify-content-between">
                                    <span class="">2 New Notifications</span>  
                                    <a href="javascript:void()" class="d-inline-block">
                                        <span class="badge badge-pill gradient-2">5</span>
                                    </a>
                                </div>
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li>
                                            <a href="javascript:void()">
                                                <span class="mr-3 avatar-icon bg-success-lighten-2"><i class="icon-present"></i></span>
                                                <div class="notification-content">
                                                    <h6 class="notification-heading">Events near you</h6>
                                                    <span class="notification-text">Within next 5 days</span> 
                                                </div>
                                            </a>
                                        </li>
                                        
                                    </ul>
                                    
                                </div>
                            </div>
                        </li>
                        <li class="icons dropdown d-none d-md-flex">
                            <a href="javascript:void(0)" class="log-user"  data-toggle="dropdown">
                                <span>English</span>  <i class="fa fa-angle-down f-s-14" aria-hidden="true"></i>
                            </a>
                            <div class="drop-down dropdown-language animated fadeIn  dropdown-menu">
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li><a href="javascript:void()">English</a></li>
                                        
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li class="icons dropdown">
                            <div class="user-img c-pointer position-relative"   data-toggle="dropdown">
                                <span class="activity active"></span>
                                <img src="images/user/form-user.png" height="40" width="40" alt="">
                            </div>
                            <div class="drop-down dropdown-profile animated fadeIn dropdown-menu">
                                <div class="dropdown-content-body">
                                    <ul>
                                        <li>
                                            <a href="app-profile.html"><i class="icon-user"></i> <span>Profile</span></a>
                                        </li>
                                        <li>
                                            <a href="javascript:void()">
                                                <i class="icon-envelope-open"></i> <span>Inbox</span> <div class="badge gradient-3 badge-pill gradient-1">3</div>
                                            </a>
                                        </li>
                                        
                                        <hr class="my-2">
                                       
                                        <li><a href="logout.php"><i class="icon-key"></i> <span>Logout</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--**********************************
            Header end ti-comment-alt
        ***********************************-->

        <!--**********************************
            Sidebar start
        ***********************************-->
        <div class="nk-sidebar">           
            <div class="nk-nav-scroll">
                <ul class="metismenu" id="menu">
                    <li class="nav-label">Champions</li>
                    
                    <li>
                        <a href="dashboard.php" aria-expanded="false">
                            <i class="icon-badge menu-icon"></i><span class="nav-text">Dashboard</span>
                        </a>
                    </li>
                 
                    <li>
                        <a href="coordinator.php" aria-expanded="false">
                            <i class="icon-badge menu-icon"></i><span class="nav-text">Coordinator</span>
                        </a>
                    </li>
                    <li>
                        <a href="champion.php" aria-expanded="false">
                            <i class="icon-badge menu-icon"></i><span class="nav-text">Champions</span>
                        </a>
                    </li>
                    <li>
                        <a href="sales.php" aria-expanded="false">
                            <i class="icon-badge menu-icon"></i><span class="nav-text">Sales</span>
                        </a>
                    </li>
                    <li>
                        <a href="registration.php" aria-expanded="false">
                            <i class="icon-badge menu-icon"></i><span class="nav-text">Login Details</span>
                        </a>
                    </li>
                    <li>
                        <a href="logout.php" aria-expanded="false">
                            <i class="icon-badge menu-icon"></i><span class="nav-text">Logout</span>
                        </a>
                    </li>
                       
                </ul>
            </div>
        </div>
        <!--**********************************
            Sidebar end
        ***********************************-->

        <!--**********************************
            Content body start
        ***********************************-->
        <div class="content-body">

            <div class="container-fluid mt-3">
                <div class="row">
                    
                <div class="col-lg-3 col-sm-6">
                        <div class="card gradient-1">
                            <div class="card-body">
                                <h3 class="card-title text-white">Champions</h3>
                                <div class="d-inline-block">
                                <h2 class="text-white">
                                        <?php
                                            $query="SELECT COUNT(*) FROM champions  ";
                                            $result = mysqli_query($con,$query);

                                            while($row = mysqli_fetch_assoc($result)){
                                                echo   $row['COUNT(*)'];
                                            }

                                        ?>
                                        </h2>
                                </div>
                                <span class="float-right display-5 opacity-5"><i class="fa fa-users"></i></span>
                            </div>
                        </div>
                    </div>
               
                </div>

                

                
<!-- row -->

<div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-validation">
                                    <form class="form-valide" method = "post" action ="champion.php">

                                        
                                    <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >Coordinator Code <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control"  name="coordinator_id" value= "<?php echo $coordinator_id?>" required placeholder="Enter a coordinators Code..">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >Champion Code <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control"  name="champions_id" value= "<?php echo $champions_id?>" required placeholder="Enter a coordinators Code..">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >First Names <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control"  name="names" value= "<?php echo $name?>" required placeholder="Enter a coordinators First Names..">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >Last Name <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control"  name="surname" value="<?php echo $surname?>" required placeholder="Enter a coordinators Last Name..">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >Phone Number <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control"  name="phone_number" value= "<?php echo $number?>" required placeholder="Enter a coordinators Phone Number..">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >Email <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control"  name="email" value= "<?php echo $email?>" required placeholder="Enter a coordinators Email ..">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >ID Number <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control"  name="id_number" value= "<?php echo $id_number?>" required placeholder="Enter a coordinators ID Number..">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >Age <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control"  name="age" value= "<?php echo $age?>" required placeholder="Enter a coordinators Age..">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >Gender <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control"  name="gender" value= "<?php echo $gender?>" required placeholder="Enter a coordinators Gender..">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >Address <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control"  name="address" value= "<?php echo $address?>" required placeholder="Enter a coordinators Address..">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >Province <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <input type="text" class="form-control"  name="province" value= "<?php echo $province?>" required placeholder="Enter a coordinators Province..">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-lg-4 col-form-label" >Qualification <span class="text-danger">*</span>
                                            </label>
                                            <div class="col-lg-6">
                                                <textarea class="form-control"  name="qualification" value= "<?php echo $qualification?>" required rows="5" placeholder="Enter a coordinators Qualifications.."></textarea>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        
                                        <div class="form-group row">
                                            <div class="col-lg-8 ml-auto">
                                                <button type="submit"name="insert" value ="SUBMIT" class="btn btn-primary">Submit</button>
                                                <button type="submit"name="update" value ="UPDATE" class="btn btn-primary">Update</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

               

               


                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                            <h3 class="card-title">Champions</h3>
                                <div class="active-member">
                                    <div class="table-responsive">
                                        <table class="table table-xs mb-0">
                                            <thead>
                                                <tr>
                                                    <th>Coordinator Id</th>
                                                    <th>Champion Id</th>
                                                    <th>Names </th>
                                                    <th>Surnames</th>
                                                    <th>Phone number</th>
                                                    <th>Email</th>
                                                    <th>ID number</th>
                                                    <th>Age</th>
                                                    <th>gender</th>
                                                    <th>Address</th>
                                                    <th>Province</th>
                                                    <th>Qualification</th>
                                                    <th>Action</th>
                                                    
                                                </tr>
                                            </thead>
                                            <tbody>
                                                

                                            <?php
                                                $sql = "SELECT * from champions ";
                                                $result = $con-> query($sql);

                                                if ($result-> num_rows > 0){
                                                    while ($row = $result-> fetch_assoc()){
                                                        echo "<tr><td>" . $row["coordinator_id"] . "</td><td>". $row["champion_id"] . "</td><td>" . $row["names"] . "</td><td>" . $row["surnames"] . "</td><td>"
                                                        . $row["phone_number"] . "</td><td>" . $row["email"] . "</td><td>" . $row["id_number"] 
                                                        . "</td><td>" . $row["age"] . "</td><td>" . $row["gender"] 
                                                        . "</td><td>" . $row["address"] . "</td><td>" . $row["province"] 
                                                        . "</td><td>" . $row["qualification"] . "</td>
                                                        <td><a href=champion.php?edit=". $row['champion_id'].">Edit</a>
                                                        <a href=champion.php?a=". $row['champion_id'].">Remove</a></td></tr>";
                                                    }
                                                        echo "</table>" ;
                                                }
                                                else
                                                {
                                                    echo "<script> alert('Results not found')</script>";
                                                }
                                            ?>
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </div>
                
               
            </div>
            <!-- #/ container -->
        </div>
        <!--**********************************
            Content body end
        ***********************************-->
        
        
        <!--**********************************
            Footer start
        ***********************************-->
        <div class="footer">
            <div class="copyright">
                <p>Copyright &copy; Designed & Developed by <a href="https://themeforest.net/user/quixlab">Digititan</a> 2021</p>
            </div>
        </div>
        <!--**********************************
            Footer end
        ***********************************-->
    </div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>

    <!-- Chartjs -->
    <script src="./plugins/chart.js/Chart.bundle.min.js"></script>
    <!-- Circle progress -->
    <script src="./plugins/circle-progress/circle-progress.min.js"></script>
    <!-- Datamap -->
    <script src="./plugins/d3v3/index.js"></script>
    <script src="./plugins/topojson/topojson.min.js"></script>
    <script src="./plugins/datamaps/datamaps.world.min.js"></script>
    <!-- Morrisjs -->
    <script src="./plugins/raphael/raphael.min.js"></script>
    <script src="./plugins/morris/morris.min.js"></script>
    <!-- Pignose Calender -->
    <script src="./plugins/moment/moment.min.js"></script>
    <script src="./plugins/pg-calendar/js/pignose.calendar.min.js"></script>
    <!-- ChartistJS -->
    <script src="./plugins/chartist/js/chartist.min.js"></script>
    <script src="./plugins/chartist-plugin-tooltips/js/chartist-plugin-tooltip.min.js"></script>



    <script src="./js/dashboard/dashboard-1.js"></script>




    <!-- line graph-->
    
    <script type="text/javascript">
    
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
            ['WEEKS', 'Sales'],

            
          
          ['WEEK1',  <?php 
          $query = "SELECT SUM(no_of_items) FROM sales ";
          $result = mysqli_query($con, $query);
  
          while($row = mysqli_fetch_assoc($result)){
              echo $row['SUM(no_of_items)'];
          }
          ?>],
          
        ]);

        var options = {
          title: 'Sales',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
      
    </script>

    <!--pie chart-->
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Target','Items'],
          
          
          ['No of T-shirts',     <?php 
          $query = "SELECT SUM(no_of_items) FROM sales  ";
          $result = mysqli_query($con, $query);
  
          while($row = mysqli_fetch_assoc($result)){
              echo $row['SUM(no_of_items)'];
              
          }
          ?>],
          ['Target',     <?php 
          $query = "SELECT SUM(target) FROM sales ";
          $result = mysqli_query($con, $query);
  
          while($row = mysqli_fetch_assoc($result)){
              echo $row['SUM(target)'];
              
          }
          ?>],
        ]);

        var options = {
          title: 'Sales'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>

</body>

</html>

<script>
$(document).ready(function(){

 load_data();

 function load_data(query)
 {
  $.ajax({
   url:"champion.php",
   method:"POST",
   data:{query:query},
   success:function(data)
   {
    $('#result').html(data);
   }
  });
 }
 $('#search_text').keyup(function(){
  var search = $(this).val();
  if(search != '')
  {
   load_data(search);
  }
  else
  {
   load_data();
  }
 });
});
</script>
