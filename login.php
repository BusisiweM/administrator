<?php
    session_start();
    include "php/dbconfig.php";
    error_reporting(E_ALL & ~E_NOTICE); 
?>


<?php

if($_SERVER["REQUEST_METHOD"] == "POST") {
    // username and password sent from form 
    $username = stripslashes($_REQUEST['username']);
    $username = mysqli_real_escape_string($con,$_POST['username']);
    $password = stripslashes($_REQUEST['password']);
    $password = mysqli_real_escape_string($con,$_POST['password']); 
    
    $sql = "SELECT * FROM admin WHERE email = '$username' and password = '$password'";
    $result = mysqli_query($con,$sql);

    if($row=mysqli_fetch_array($result)){
        $_SESSION['username'] = $row['username'];
        $_SESSION['email'] = $row['email'];
        echo "<script>window.location.href='dashboard.php';</script>";
        
    }else {
        echo "<script> alert('Incorrect password or username, enter your details again')</script>";
    }
 }	 
	 
?>

<!DOCTYPE html>
<html class="h-100" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>D4IR Initiative</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous"> -->
    <link href="css/style.css" rel="stylesheet">
    
</head>

<body class="h-100">
    
    <!--*******************
        Preloader start
    ********************-->
    <div id="preloader">
        <div class="loader">
            <svg class="circular" viewBox="25 25 50 50">
                <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="3" stroke-miterlimit="10" />
            </svg>
        </div>
    </div>
    <!--*******************
        Preloader end
    ********************-->

    



    <div class="login-form-bg h-100">
        <div class="container h-100">
            <div class="row justify-content-center h-100">
                <div class="col-xl-6">
                    <div class="form-input-content">
                        <div class="card login-form mb-0">
                            <div class="card-body pt-5">
                                <a class="text-center" href="index.html"> <h4>Welcome Admin</h4></a>
        
                                <form class="mt-5 mb-5 login-input"  method="post">
                                    <div class="form-group">
                                        <input type="email" name="username" required class="form-control" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" required class="form-control" placeholder="Password">
                                    </div>
                                    <button class="btn login-form__btn submit w-100" value ="LOGIN" >Sign In</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    

    <!--**********************************
        Scripts
    ***********************************-->
    <script src="plugins/common/common.min.js"></script>
    <script src="js/custom.min.js"></script>
    <script src="js/settings.js"></script>
    <script src="js/gleek.js"></script>
    <script src="js/styleSwitcher.js"></script>
</body>
</html>





